package com.telerikacademy.web.deliverit.models;

import javax.persistence.*;
import javax.validation.constraints.Positive;
import java.util.Objects;

@Entity
@Table(name = "parcels")
public class Parcel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "parcel_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Positive(message = "Weight should be positive!")
    private Double weight;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "warehouse_id")
    private Warehouse warehouse;

    @ManyToOne
    @JoinColumn(name = "shipment_id")
    private Shipment shipment;

    public Parcel() {}

    public Parcel(Long id, Category category, Customer customer, Warehouse warehouse,
                  double weight, Shipment shipment) {
        setId(id);
        setCategory(category);
        setCustomer(customer);
        setWarehouse(warehouse);
        setWeight(weight);
        setShipment(shipment);
    }

    public Long getId() {
        return id;
    }

    public Shipment getShipment() {
        return shipment;
    }

    public void setShipment(Shipment shipment) {
        this.shipment = shipment;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Parcel parcel = (Parcel) o;
        return Objects.equals(id, parcel.id) && Objects.equals(category, parcel.category) && Objects.equals(weight, parcel.weight) && Objects.equals(customer, parcel.customer) && Objects.equals(warehouse, parcel.warehouse) && Objects.equals(shipment, parcel.shipment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, category, weight, customer, warehouse, shipment);
    }
}
