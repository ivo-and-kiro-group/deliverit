package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Address;
import com.telerikacademy.web.deliverit.models.Customer;
import com.telerikacademy.web.deliverit.models.Customer;
import com.telerikacademy.web.deliverit.repositories.contracts.CustomerRepository;
import com.telerikacademy.web.deliverit.services.contracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository repository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Customer> getAll(@RequestParam(required = false) Optional<String> firstName,
                                 @RequestParam(required = false) Optional<String> lastName,
                                 @RequestParam(required = false) Optional<String> email) {
        return repository.getAll(firstName, lastName, email);
    }

    @Override
    public Customer getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public Customer getByUsername(String username) {
        return repository.getByUsername(username);
    }

    @Override
    public Customer getByEmail(String email) {
        return repository.getByEmail(email);
    }

    @Override
    public Customer getByAddress(Address address) {
        return repository.getByAddress(address);
    }

    @Override
    public Customer create(Customer customer) {
        boolean duplicateEmailExists = true;
        boolean duplicateUsernameExists = true;
        boolean duplicateAddressExists = true;

        try {
            repository.getByEmail(customer.getUser().getEmail());
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        if (duplicateEmailExists) {
            throw new DuplicateEntityException("Customer", "email", customer.getUser().getEmail());
        }

        try {
            repository.getByUsername(customer.getUser().getUsername());
        } catch (EntityNotFoundException e) {
            duplicateUsernameExists = false;
        }

        if (duplicateUsernameExists) {
            throw new DuplicateEntityException("Customer", "username", customer.getUser().getUsername());
        }

        try {
            repository.getByAddress(customer.getAddress());

        } catch (EntityNotFoundException e) {
            duplicateAddressExists = false;
        }

        if (duplicateAddressExists) {
            throw new DuplicateEntityException("Address", "street", customer.getAddress().getStreet());
        }

        return repository.create(customer);
    }

    @Override
    public Customer update(Customer customer) {
        boolean duplicateEmailExists = true;
        boolean duplicateUsernameExists = true;
        boolean duplicateAddressExists = true;
        Customer existingCustomer;

        try {
            existingCustomer = repository.getByEmail(customer.getUser().getEmail());
            if (existingCustomer.getId().equals(customer.getId())) {
                duplicateEmailExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        if (duplicateEmailExists) {
            throw new DuplicateEntityException("Customer", "email", customer.getUser().getEmail());
        }

        try {
            existingCustomer = repository.getByUsername(customer.getUser().getUsername());
            if (existingCustomer.getId().equals(customer.getId())) {
                duplicateUsernameExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateUsernameExists = false;
        }

        if (duplicateUsernameExists) {
            throw new DuplicateEntityException("Customer", "username", customer.getUser().getUsername());
        }

        try {
            existingCustomer = repository.getByAddress(customer.getAddress());
            if (existingCustomer.getId().equals(customer.getId())) {
                duplicateAddressExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateAddressExists = false;
        }

        if (duplicateAddressExists) {
            throw new DuplicateEntityException("Customer", "address", customer.getAddress().toString());
        }

        return repository.update(customer);
    }

    @Override
    public Customer delete(Long id) {
        return repository.delete(id);
    }

    @Override
    public Page<Customer> findPaginated(Pageable pageable) {

        List<Customer> allCustomers = getAll(Optional.empty(), Optional.empty(), Optional.empty());

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Customer> list;

        if (allCustomers.size()< startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allCustomers.size());
            list = allCustomers.subList(startItem, toIndex);
        }

        Page<Customer> customerPage
                = new PageImpl<Customer>(list, PageRequest.of(currentPage, pageSize), allCustomers.size());

        return customerPage;
    }
}
