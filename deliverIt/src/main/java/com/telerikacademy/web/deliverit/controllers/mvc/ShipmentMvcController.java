package com.telerikacademy.web.deliverit.controllers.mvc;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exceptions.DeletingEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exceptions.LocalDateException;
import com.telerikacademy.web.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.*;
import com.telerikacademy.web.deliverit.repositories.contracts.UserRepository;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import com.telerikacademy.web.deliverit.services.contracts.ShipmentService;
import com.telerikacademy.web.deliverit.services.modelMappers.ShipmentModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.telerikacademy.web.deliverit.controllers.AuthenticationHelper.EMPLOYEE_ROLE;

@Controller
@RequestMapping("/shipments")
public class ShipmentMvcController extends BasicAuthenticationMvcController {
    private final ShipmentService shipmentService;
    private final ParcelService parcelService;
    private final UserRepository userRepository;
    private final ShipmentModelMapper shipmentModelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ShipmentMvcController(ShipmentService shipmentService,
                                 ParcelService parcelService,
                                 UserRepository userRepository,
                                 ShipmentModelMapper shipmentModelMapper,
                                 AuthenticationHelper authenticationHelper) {
        super(authenticationHelper);
        this.shipmentService = shipmentService;
        this.parcelService = parcelService;
        this.userRepository = userRepository;
        this.shipmentModelMapper = shipmentModelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showAllShipments(Model model,
                                   @RequestParam("page") Optional<Integer> page,
                                   @RequestParam("size") Optional<Integer> size,
                                   HttpSession session) {

        try {
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }


        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<Shipment> shipmentPage = shipmentService.findPaginated(PageRequest.of(currentPage - 1, pageSize));
        List<Shipment> shipments = shipmentPage.getContent();

        model.addAttribute("shipments", shipments);
        model.addAttribute("shipmentSearchDto", new ShipmentSearchDto());
        model.addAttribute("shipmentPage", shipmentPage);

        int totalPages = shipmentPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "shipments";
    }

    @GetMapping("/{id}")
    public String showSingleShipment(@PathVariable Long id, Model model,
                                     @RequestParam("page") Optional<Integer> page,
                                     @RequestParam("size") Optional<Integer> size,
                                     HttpSession session) {
        try {
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);

            Shipment shipment = shipmentService.getById(id);
            List<Parcel> shipmentParcels = shipmentService.getShipmentParcels(id);

            shipment.calcStatus(shipmentParcels);

            int currentPage = page.orElse(1);
            int pageSize = size.orElse(5);

            Page<Parcel> parcelPage = parcelService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

            model.addAttribute("parcelPage", parcelPage);
            model.addAttribute("shipment", shipment);
            model.addAttribute("shipmentParcels", shipmentParcels);

            int totalPages = parcelPage.getTotalPages();
            if (totalPages > 0) {
                List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                        .boxed()
                        .collect(Collectors.toList());
                model.addAttribute("pageNumbers", pageNumbers);
            }
            return "shipment";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }
    }

    @GetMapping("/new")
    public String showNewShipmentPage(Model model, HttpSession session) {
        try {
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }

        model.addAttribute("shipment", new ShipmentDto());
        return "shipment-new";
    }

    @PostMapping("/new")
    public String createShipment(@Valid @ModelAttribute("shipment") ShipmentDto shipmentDto, BindingResult errors) {
        if (errors.hasErrors()) {
            return "shipment-new";
        }

        try {
            Shipment newShipment = shipmentModelMapper.fromDto(shipmentDto);
            shipmentService.create(newShipment);
            return "redirect:/shipments";
        } catch (LocalDateException e) {
            if (e.getMessage().contains("departure date")) {
                errors.rejectValue("departureDate", "Invalid departureDate", e.getMessage());
                return "shipment-new";
            }
            errors.rejectValue("arrivalDate", "Invalid arrivalDate", e.getMessage());
            return "shipment-new";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditShipmentPage(@PathVariable Long id, Model model, HttpSession session) {
        try {
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);

            Shipment shipmentToUpdate = shipmentService.getById(id);
            ShipmentDto shipmentDto = shipmentModelMapper.toDto(shipmentToUpdate);
            model.addAttribute("shipment", shipmentDto);
            return "shipment-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PostMapping("/{id}/update")
    public String updateShipment(@PathVariable Long id,
                                 @Valid @ModelAttribute("shipment") ShipmentDto shipmentDto,
                                 BindingResult errors,
                                 HttpSession session) {
        if (errors.hasErrors()) {
            return "shipment-update";
        }

        try {
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
            Shipment newShipment = shipmentModelMapper.fromDto(shipmentDto, id);
            shipmentService.update(newShipment);
            return "redirect:/shipments";
        } catch (LocalDateException e) {
            if (e.getMessage().contains("departure date")) {
                errors.rejectValue("departureDate", "Invalid departureDate", e.getMessage());
                return "shipment-update";
            }
            errors.rejectValue("arrivalDate", "Invalid arrivalDate", e.getMessage());
            return "shipment-update";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteShipment(@PathVariable Long id, Model model, HttpSession session) {
        try {
            // Authentication
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
            shipmentService.delete(id);
            return "redirect:/shipments";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (DeletingEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "delete-warning";
        }
    }

    @GetMapping("/search")
    public String handleSearchShipment() {
        return "redirect:/shipments";
    }

    @PostMapping("/search")
    public String handleShipmentSearch(Model model,
                                        @ModelAttribute("shipmentSearchDto") ShipmentSearchDto shipmentSearchDto) {
        model.addAttribute("shipments", shipmentService.filter(shipmentModelMapper.fromDto(shipmentSearchDto)));

        return "shipments";
    }
}
