package com.telerikacademy.web.deliverit.repositories;

import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.repositories.contracts.CategoryRepository;
import com.telerikacademy.web.deliverit.models.Category;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class CategoryRepositoryImpl implements CategoryRepository {

    private final SessionFactory sessionFactory;

    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Category> getAll(Optional<String> name) {

        try (Session session = sessionFactory.openSession()) {
            Query<Category> query = session.createQuery("from Category ", Category.class);
            /*query.setParameter("name", name.orElse(""));*/

            return query.list();
        }
    }


    @Override
    public Category getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Category> query = session.createQuery("from Category where name = :name", Category.class);
            query.setParameter("name", name);

            List<Category> result = query.list();

            if (result.size() == 0) {
                throw new EntityNotFoundException("Category", "name", name);
            }

            return result.get(0);
        }
    }

    @Override
    public Category getById(Long id) {
        try (Session session = sessionFactory.openSession()) {
            Category category = session.get(Category.class, id);
            if (category == null) {
                throw new EntityNotFoundException("Category", id);
            }

            return category;
        }
    }

    @Override
    public Category create(Category category) {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.save(category);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
        return category;
    }

    @Override
    public Category update(Category category) {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.update(category);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }

        return category;
    }


    @Override
    public Category delete(Category category) {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.delete(category);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
        return category;
    }
}
