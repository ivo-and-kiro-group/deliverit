package com.telerikacademy.web.deliverit.models;

import javax.validation.constraints.*;

public class AddressDto {

    @NotEmpty
    @Size(min = 2, max = 20, message = "Name should be between 2 and 20 symbols")
    private String street;

    @NotNull
    @Positive(message = "CityId should be positive.")
    private Long cityId;

    public AddressDto() {
    }

    public AddressDto(@NotEmpty @Size(min = 2, max = 20, message = "Name should be between 2 and 20 symbols") String street,
                      @NotNull @Positive(message = "CityId should be positive.") Long cityId) {
        this.street = street;
        this.cityId = cityId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }
}
