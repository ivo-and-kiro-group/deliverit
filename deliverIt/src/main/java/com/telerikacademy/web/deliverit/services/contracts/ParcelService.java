package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.Customer;
import com.telerikacademy.web.deliverit.models.Employee;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

public interface ParcelService {
   List<Parcel> getAll(@RequestParam(required = false) Optional<String> customerUsername,
                               @RequestParam(required = false) Optional<String> warehouseCityName,
                               @RequestParam(required = false) Optional<Double> weight,
                               @RequestParam(required = false) Optional<String> category,
                               @RequestParam(required = false) Optional<Boolean> orderByWeight,
                               @RequestParam(required = false) Optional<Boolean> orderByDepartureDate);

   List<Parcel> getShipmentParcels(Long shipmentId);

   List<Parcel> getCustomerParcels(Long customerId);

   Parcel getById(Long id);

   Parcel getById(Long id,Customer customer);

   Parcel create(Parcel Parcel);

   Parcel update(Parcel Parcel);

   void delete(Long id);

   Page<Parcel> findPaginated(Pageable pageable);

   Page<Parcel> findPaginated(Pageable pageable,Long customerId);

}
