package com.telerikacademy.web.deliverit.services.modelMappers;


import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.ShipmentDto;
import com.telerikacademy.web.deliverit.models.ShipmentSearchDto;
import com.telerikacademy.web.deliverit.models.ShipmentSearchParameters;
import com.telerikacademy.web.deliverit.repositories.contracts.ShipmentRepository;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import com.telerikacademy.web.deliverit.services.contracts.ShipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ShipmentModelMapper {

    private final ShipmentRepository repository;

    @Autowired
    public ShipmentModelMapper(ShipmentRepository repository) {
        this.repository = repository;
    }

    public ShipmentSearchParameters fromDto(ShipmentSearchDto dto) {
        return new ShipmentSearchParameters(dto.getArrivalDate());
    }

    public Shipment fromDto(ShipmentDto shipmentDto) {
        Shipment shipment = new Shipment();
        dtoToObject(shipmentDto, shipment);
        return shipment;
    }

    public Shipment fromDto(ShipmentDto shipmentDto, Long id) {
        Shipment shipment = repository.getById(id);
        dtoToObject(shipmentDto, shipment);
        return shipment;
    }

    public ShipmentDto toDto(Shipment shipment) {
        ShipmentDto shipmentDto = new ShipmentDto();
        shipmentDto.setDepartureDate(shipment.getDepartureDate());
        shipmentDto.setArrivalDate(shipment.getArrivalDate());
        return shipmentDto;
    }

    private void dtoToObject(ShipmentDto shipmentDto, Shipment shipment) {
        shipment.setDepartureDate(shipmentDto.getDepartureDate());
        shipment.setArrivalDate(shipmentDto.getArrivalDate());
    }


}