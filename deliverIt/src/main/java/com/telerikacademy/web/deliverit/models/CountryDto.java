package com.telerikacademy.web.deliverit.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CountryDto {

    @NotEmpty
    @Size(min = 2, max = 20, message = "Name should be between 2 and 20 symbols")
    private String name;

    public CountryDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
