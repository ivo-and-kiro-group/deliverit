package com.telerikacademy.web.deliverit.controllers.mvc;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.Customer;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.services.contracts.CustomerService;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.telerikacademy.web.deliverit.controllers.AuthenticationHelper.EMPLOYEE_ROLE;

@Controller
@RequestMapping("/customers")
public class CustomerMvcController extends BasicAuthenticationMvcController{

    private final CustomerService customerService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CustomerMvcController(CustomerService customerService, AuthenticationHelper authenticationHelper) {
        super(authenticationHelper);
        this.customerService = customerService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showAllCustomers(Model model,
                                   @RequestParam("page") Optional<Integer> page,
                                   @RequestParam("size") Optional<Integer> size,
                                   HttpSession session) {

        try {
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }

        List<Customer> customers = customerService.getAll(Optional.empty(),
                Optional.empty(),
                Optional.empty());
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<Customer> customerPage = customerService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

        model.addAttribute("customerPage", customerPage);

        int totalPages = customerPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "customers";
    }

}