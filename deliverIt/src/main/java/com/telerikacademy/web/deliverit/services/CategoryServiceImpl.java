package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Category;
import com.telerikacademy.web.deliverit.repositories.contracts.CategoryRepository;
import com.telerikacademy.web.deliverit.services.contracts.CategoryService;
import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository repository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Category> getAll(Optional<String> name) {
        return repository.getAll(name);
    }

    @Override
    public Category getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public Category create(Category category) {
        boolean duplicateExists = true;

        try {
            Category existingCategory = repository.getByName(category.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Category", "name", category.getName());
        }

        return repository.create(category);
    }

    @Override
    public Category update(Category category) {
        boolean duplicateExists = true;

        try {
            Category existingCategory = repository.getByName(category.getName());
            if (existingCategory.getId().equals(category.getId())) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Category", "name", category.getName());
        }

        return repository.update(category);
    }

    @Override
    public Category delete(Long id) {
        Category categoryToDelete = getById(id);
        return repository.delete(categoryToDelete);
    }
}
