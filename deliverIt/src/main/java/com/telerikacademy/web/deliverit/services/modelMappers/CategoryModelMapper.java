package com.telerikacademy.web.deliverit.services.modelMappers;

import com.telerikacademy.web.deliverit.models.Category;
import com.telerikacademy.web.deliverit.models.CategoryDto;
import com.telerikacademy.web.deliverit.repositories.contracts.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CategoryModelMapper {
    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryModelMapper(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Category fromDto(CategoryDto categoryDto){
        Category category = new Category();
        dtoToObject(categoryDto,category);
        return category;
    }

    public Category fromDto(CategoryDto categoryDto,Long id){
        Category category = categoryRepository.getById(id);
        dtoToObject(categoryDto,category);
        return category;
    }

    private void dtoToObject(CategoryDto categoryDto, Category category){
        category.setName(categoryDto.getName());
    }

}
