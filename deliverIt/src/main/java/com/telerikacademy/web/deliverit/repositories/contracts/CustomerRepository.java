package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.Address;
import com.telerikacademy.web.deliverit.models.Customer;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository {
    List<Customer> getAll(Optional<String> firstName,
                          Optional<String> lastName,
                          Optional<String> email);

    Customer getById(Long id);

    Customer getByEmail(String email);

    Customer getByUsername(String username);

    Customer getByAddress(Address address);

    Customer create(Customer customer);

    Customer update(Customer customer);

    Customer delete(Long id);

}
