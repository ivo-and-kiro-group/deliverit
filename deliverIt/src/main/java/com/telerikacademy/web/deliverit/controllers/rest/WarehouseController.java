package com.telerikacademy.web.deliverit.controllers.rest;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.models.WarehouseDto;
import com.telerikacademy.web.deliverit.models.WarehouseSearchParameters;
import com.telerikacademy.web.deliverit.services.contracts.WarehouseService;
import com.telerikacademy.web.deliverit.services.modelMappers.WarehouseModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/warehouses")
public class WarehouseController {

    private final WarehouseService service;
    private final WarehouseModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public WarehouseController(WarehouseService service, WarehouseModelMapper modelMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Warehouse> getAll() {
        return service.getAll();
    }

    @GetMapping("/filter")
    public List<Warehouse> filter(@RequestParam(required = false) String street,
                                  @RequestParam(required = false) Long cityId) {
        return service.filter(new WarehouseSearchParameters(street, cityId));
    }

    @GetMapping("/{id}")
    public Warehouse getById(@RequestHeader HttpHeaders headers, @Valid @PathVariable Long id) {
        try {
            authenticationHelper.tryGetEmployee(headers);
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Warehouse create(@RequestHeader HttpHeaders headers, @RequestBody @Valid WarehouseDto warehouseDto) {
        try {
            authenticationHelper.tryGetEmployee(headers);
            Warehouse warehouse = modelMapper.fromDto(warehouseDto);
            return service.create(warehouse);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Warehouse update(@RequestHeader HttpHeaders headers,@PathVariable Long id, @Valid @RequestBody WarehouseDto warehouseDto) {
        try {
            authenticationHelper.tryGetEmployee(headers);
            Warehouse warehouse = modelMapper.fromDto(warehouseDto,id);
            return service.update(warehouse);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,@PathVariable Long id){
        try {
            authenticationHelper.tryGetEmployee(headers);
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

