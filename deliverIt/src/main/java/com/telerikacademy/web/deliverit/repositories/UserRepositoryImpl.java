package com.telerikacademy.web.deliverit.repositories;

import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Employee;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<User> getAll(Optional<String> firstName, Optional<String> lastName) {
                     /* String q = "";
        if(firstName.isPresent()) {
            q = "where u.firstName = :firstName";
        }
        if(lastName.isPresent()){
            q += "and u.lastName= :lastName";
        }*/

        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User as u " +
                    "where u.firstName like concat('%', :firstName,'%') and u.lastName like concat('%', :lastName,'%') ", User.class);
            query.setParameter("firstName", firstName.orElse(""));
            query.setParameter("lastName", lastName.orElse(""));
            return query.list();
        }
    }

    @Override
    public User getById(Long id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User as u " +
                    "where u.email = :email", User.class);
            query.setParameter("email",email);
            List<User> users = query.list();

            if (users.isEmpty()) {
                throw new EntityNotFoundException("User", "email", email);
            }

            return query.list().get(0);
        }
    }

    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User as u " +
                    "where u.username = :username", User.class);
            query.setParameter("username",username);
            List<User> users = query.list();

            if (users.isEmpty()) {
                throw new EntityNotFoundException("User", "username", username);
            }

            return query.list().get(0);
        }
    }

    @Override
    public User create(User user) {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
           // tx = session.beginTransaction();
            session.save(user);
           // tx.commit();
            return user;
        } /*catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }*/
    }

    @Override
    public User update(User user) {
        Transaction tx = null;

        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.update(user);
            tx.commit();
            return user;
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }
    }

    @Override
    public User delete(Long id) {
        Transaction tx = null;
        User user = getById(id);

        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.delete(user);
            tx.commit();
            return user;
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }
    }
}
