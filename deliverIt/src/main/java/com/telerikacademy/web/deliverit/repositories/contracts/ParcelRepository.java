package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.Parcel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

public interface ParcelRepository {

    List<Parcel> getAll(Optional<String> customerUsername,
                        Optional<String> warehouseCityName,
                        Optional<Double> weight,
                        Optional<String> category,
                        Optional<Boolean> orderByWeight,
                        Optional<Boolean> orderByDepartureDate);

    List<Parcel> getShipmentParcels(Long shipmentId);

    List<Parcel> getCustomerParcels(Long customerId);

    Parcel getById(Long id);

    Parcel create(Parcel Parcel);

    Parcel update(Parcel Parcel);

    void delete(Long id);

    boolean isParcelExist(Long warehouseId);
}
