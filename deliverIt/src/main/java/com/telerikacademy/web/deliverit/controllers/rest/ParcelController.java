package com.telerikacademy.web.deliverit.controllers.rest;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exceptions.*;
import com.telerikacademy.web.deliverit.models.*;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import com.telerikacademy.web.deliverit.services.contracts.ShipmentService;
import com.telerikacademy.web.deliverit.services.modelMappers.ParcelModelMapper;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/parcels")
public class ParcelController {

    private final ParcelService service;
    private final ParcelModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ParcelController(ParcelService service,
                            ParcelModelMapper modelMapper,
                            AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Parcel> getAll(@RequestHeader HttpHeaders headers,
                               @RequestParam(required = false) Optional<String> customerUsername,
                               @RequestParam(required = false) Optional<String> warehouseCityName,
                               @RequestParam(required = false) Optional<Double> weight,
                               @RequestParam(required = false) Optional<String> category,
                               @RequestParam(required = false) Optional<Boolean> orderByWeight,
                               @RequestParam(required = false) Optional<Boolean> orderByDepartureDate) {
        try {
            authenticationHelper.tryGetEmployee(headers);
            return service.getAll(customerUsername, warehouseCityName, weight, category,orderByWeight,
                    orderByDepartureDate);
        } catch (ResponseStatusException ignored) {
            Customer customer = authenticationHelper.tryGetCustomer(headers);
            return service.getAll(Optional.of(customer.getUser().getUsername()), warehouseCityName, weight, category,orderByWeight,
                    orderByDepartureDate);
        }
    }

    @GetMapping("/{id}")
    public Parcel getById(@RequestHeader HttpHeaders headers, @PathVariable Long id) {
        try {
            Customer customer = authenticationHelper.tryGetCustomer(headers);
            return service.getById(id,customer);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage());
        } catch (ResponseStatusException ex){
            authenticationHelper.tryGetEmployee(headers);
            return service.getById(id);
        }
    }

    @PostMapping
    public Parcel create(@RequestHeader HttpHeaders headers,
                         @Valid @RequestBody ParcelDto parcelDto) {
        try {
            authenticationHelper.tryGetEmployee(headers);
            Parcel parcel = modelMapper.fromDto(parcelDto);;
            service.create(parcel);
            return parcel;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException | LocalDateException | DeletingEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Parcel update(@RequestHeader HttpHeaders headers,
                         @PathVariable Long id,
                         @Valid @RequestBody ParcelDto parcelDto) {
        try {
            Employee employee = authenticationHelper.tryGetEmployee(headers);
            Parcel parcel = modelMapper.fromDto(parcelDto, id);
            return service.update(parcel);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable Long id) {
        try {
            authenticationHelper.tryGetEmployee(headers);
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException | DeletingEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
