package com.telerikacademy.web.deliverit.exceptions;

public class LocalDateException extends RuntimeException {
    public LocalDateException(String message) {
        super(message);
    }
}
