package com.telerikacademy.web.deliverit.models;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class CustomerDto {

    @Valid
    private UserDto user;

    @Valid
    private AddressDto address;

    public CustomerDto() {
    }

    public CustomerDto(UserDto user, AddressDto address) {
        setUser(user);
        setAddress(address);
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }
}
