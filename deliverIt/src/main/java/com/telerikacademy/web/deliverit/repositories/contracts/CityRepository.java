package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.City;

import java.util.List;
import java.util.Optional;

public interface CityRepository {
    List<City> getAll();

    City getById(Long id);

    City getByName(String name);

}
