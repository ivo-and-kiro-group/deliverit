package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    List<Category> getAll(Optional<String> name);

    Category getById(Long id);

    Category create(Category category);

    Category update(Category category);

    Category delete(Long id);
}
