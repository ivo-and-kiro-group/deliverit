package com.telerikacademy.web.deliverit.repositories;

import com.sun.xml.bind.v2.TODO;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.ShipmentSearchParameters;
import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.repositories.contracts.ShipmentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.telerikacademy.web.deliverit.repositories.QueryHelpers.like;

@Repository
@Transactional
public class ShipmentRepositoryImpl implements ShipmentRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ShipmentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Shipment> getAll(){

        try(Session session = sessionFactory.openSession()){
            var query = session.createQuery("from Shipment ", Shipment.class);
            return query.list();
        }
    }

    @Override
    public List<Shipment> filter(ShipmentSearchParameters ssp) {
        try (Session session = sessionFactory.openSession()) {
           var query = session.createQuery("from Shipment where arrivalDate =: arrivalDate", Shipment.class);
           query.setParameter("arrivalDate", ssp.getArrivalDate());

            return query.list();
        }
    }


    @Override
    public Shipment getById(Long id) {
        try (Session session = sessionFactory.openSession()) {
            Shipment shipment = session.get(Shipment.class, id);
            if (shipment == null) {
                throw new EntityNotFoundException("Shipment", id);
            }
            return shipment;
        }
    }

    @Override
    public Shipment create(Shipment shipment) {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.save(shipment);
            tx.commit();
            return shipment;
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }
    }

    @Override
    public Shipment update(Shipment shipment) {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.update(shipment);
            tx.commit();
            return shipment;
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }

            throw e;
        }
    }

    @Override
    public void delete(Shipment shipmentToDelete) {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.delete(shipmentToDelete);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }
    }

}
