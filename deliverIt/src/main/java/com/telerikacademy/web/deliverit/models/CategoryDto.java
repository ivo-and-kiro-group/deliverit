package com.telerikacademy.web.deliverit.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CategoryDto {
    @NotNull(message = "Name cannot be null!")
    @Size(min = 3, max = 20, message = "Name should be between 3 and 20 symbols!")
    private String name;

    public CategoryDto() {
    }

    public CategoryDto( String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
