package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.models.WarehouseSearchParameters;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

public interface WarehouseRepository {
    List<Warehouse> getAll();

    List<Warehouse> filter(WarehouseSearchParameters wsp);

    Warehouse getById(Long id);

    List<Warehouse> getByStreet(String street);

    Warehouse getByStreetAndCity(String street, City city);

    Warehouse create(Warehouse warehouse);

    Warehouse update(Warehouse warehouse);

    Warehouse delete(Long id);
}
