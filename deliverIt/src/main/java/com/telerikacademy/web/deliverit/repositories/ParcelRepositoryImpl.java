package com.telerikacademy.web.deliverit.repositories;

import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.*;

import com.telerikacademy.web.deliverit.repositories.contracts.ParcelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
@Transactional
public class ParcelRepositoryImpl implements ParcelRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public ParcelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Parcel> getAll(Optional<String> customerUsername,
                               Optional<String> warehouseCityName,
                               Optional<Double> weight,
                               Optional<String> category,
                               Optional<Boolean> orderByWeight,
                               Optional<Boolean> orderByDepartureDate) {

        String q = "";
        if (weight.isPresent()) {
            q = "AND p.weight = :weight";
        }

        String q1 = "";

        if(orderByWeight.isPresent()){
            if(orderByWeight.get()){
                q1 = q1 + "order by weight desc" ;
            }
        }

        if(orderByDepartureDate.isPresent()){
            if(orderByDepartureDate.get() && orderByWeight.isPresent() && orderByWeight.get()){
                q1 = q1 + "order by weight and shipment.departureDate desc" ;
            }
            if(orderByDepartureDate.get()){
                q1 = q1 + "order by p.shipment.departureDate desc" ;
            }
        }
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery(
                    "from Parcel as p where p.customer.user.username like concat('%', :customerUsername, '%') " +
                            "AND p.warehouse.address.city.name like concat('%', :warehouseCityName, '%') " +
                            "AND p.category.name like concat('%', :category, '%') " + q + q1, Parcel.class);
            query.setParameter("customerUsername", customerUsername.orElse(""));
            query.setParameter("warehouseCityName", warehouseCityName.orElse(""));
            query.setParameter("category", category.orElse(""));
            if (!q.equals("")) {
                query.setParameter("weight", weight.get());
            }

            return query.list();
        }
    }

    @Override
    public List<Parcel> getCustomerParcels(Long customerId) {
        try (Session session = sessionFactory.openSession()){
            Query<Parcel> query = session.createQuery("from Parcel " +
                    "where customer.id =: customerId", Parcel.class);
            query.setParameter("customerId", customerId);
            return query.list();
        }
    }

    @Override
    public List<Parcel> getShipmentParcels(Long shipmentId) {
        try (Session session = sessionFactory.openSession()) {
            var query= session.createQuery("from Parcel as p " +
                    "where p.shipment.id = :shipmentId", Parcel.class);
            query.setParameter("shipmentId", shipmentId);
            return query.list();
        }
    }

    @Override
    public Parcel getById(Long id) {
        try (Session session = sessionFactory.openSession()) {
            Parcel parcel = session.get(Parcel.class, id);
            if (parcel == null) {
                throw new EntityNotFoundException("Parcel", id);
            }
            return parcel;
        }
    }

    @Override
    public boolean isParcelExist(Long warehouseId) {
        try (Session session = sessionFactory.openSession()){
            var query = session.createQuery("from Parcel as p " +
                    "where p.warehouse.id = :warehouseId", Parcel.class).setParameter("warehouseId", warehouseId);

            return !query.list().isEmpty();
        }
    }

    @Override
    public Parcel create(Parcel parcel) {
        Transaction tx = null;

        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.save(parcel);
            tx.commit();
            return parcel;
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }
    }

    @Override
    public Parcel update(Parcel parcel) {
        Transaction tx = null;

        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.update(parcel);
            tx.commit();
            return parcel;
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }
    }

    @Override
    public void delete(Long id) {
        Transaction tx = null;

        Parcel parcel = getById(id);
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.delete(parcel);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }
    }

}

