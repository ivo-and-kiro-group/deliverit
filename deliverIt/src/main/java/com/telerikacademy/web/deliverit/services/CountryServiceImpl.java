package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.models.Country;
import com.telerikacademy.web.deliverit.repositories.contracts.CountryRepository;
import com.telerikacademy.web.deliverit.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository repository;

    @Autowired
    public CountryServiceImpl(CountryRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Country> getAll() {
        return repository.getAll();
    }

    @Override
    public Country getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public Country getByName(String name) {
        return repository.getByName(name);
    }


}
