package com.telerikacademy.web.deliverit.models.enums;

public enum Status {

    PREPARING("Preparing"),
    ONTHEWAY("On the way"),
    COMPLETED("Completed");

    private String status;
    private Long id;

    Status(String status){
        this.status=status;
    }

    @Override
    public String toString() {
        return this.status;
    }
}
