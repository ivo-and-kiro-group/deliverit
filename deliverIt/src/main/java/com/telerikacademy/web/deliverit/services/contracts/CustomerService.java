package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.Address;
import com.telerikacademy.web.deliverit.models.Customer;
import com.telerikacademy.web.deliverit.models.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
    List<Customer> getAll(@RequestParam(required = false) Optional<String> firstName,
                          @RequestParam(required = false) Optional<String> lastName,
                          @RequestParam(required = false) Optional<String> email);

    Customer getById(Long id);

    Customer getByUsername(String username);

    Customer getByEmail(String email);

    Customer getByAddress(Address address);

    Customer create(Customer customer);

    Customer update(Customer customer);

    Customer delete(Long id);

    Page<Customer> findPaginated(Pageable pageable);
}
