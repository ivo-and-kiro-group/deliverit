package com.telerikacademy.web.deliverit.controllers.mvc;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exceptions.DeletingEntityException;
import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.*;
import com.telerikacademy.web.deliverit.services.contracts.EmployeeService;
import com.telerikacademy.web.deliverit.services.modelMappers.EmployeeModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.telerikacademy.web.deliverit.controllers.AuthenticationHelper.EMPLOYEE_ROLE;
import static com.telerikacademy.web.deliverit.controllers.AuthenticationHelper.MANAGER_ROLE;

@Controller
@RequestMapping("/employees")
public class EmployeeMvcController extends BasicAuthenticationMvcController{

    private final EmployeeService employeeService;
    private final AuthenticationHelper authenticationHelper;
    private final EmployeeModelMapper employeeModelMapper;

    @Autowired
    public EmployeeMvcController(EmployeeService employeeService,
                                 AuthenticationHelper authenticationHelper,
                                 EmployeeModelMapper employeeModelMapper) {
        super(authenticationHelper);
        this.employeeService = employeeService;
        this.authenticationHelper = authenticationHelper;
        this.employeeModelMapper = employeeModelMapper;
    }

    @GetMapping
    public String showAllEmployees(Model model,
                                   @RequestParam("page") Optional<Integer> page,
                                   @RequestParam("size") Optional<Integer> size,
                                   HttpSession session) {
        try {
            authenticationHelper.verifyAuthorization(session, MANAGER_ROLE);
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }

        List<Employee> employees = employeeService.getAll(Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<Employee> employeePage = employeeService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

        model.addAttribute("employeePage", employeePage);

        int totalPages = employeePage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "employees";
    }

    @GetMapping("/new")
    public String showNewEmployeePage(Model model, HttpSession session) {
        try {
            authenticationHelper.verifyAuthorization(session, MANAGER_ROLE);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }

        model.addAttribute("employee", new EmployeeDto());
        return "employee-new";
    }


    @PostMapping("/new")
    public String createEmployee(@Valid @ModelAttribute("employee") EmployeeDto employeeDto,
                                 BindingResult errors,
                                 Model model,
                                 HttpSession session) {
        if (errors.hasErrors()) {
            return "employee-new";
        }

        try {
            authenticationHelper.verifyAuthorization(session, MANAGER_ROLE);
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }

        try {
            Employee employee = employeeModelMapper.fromDto(employeeDto);
            employeeService.create(employee);

            return "redirect:/employees";
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("username")) {
                errors.rejectValue("username", "duplicate_username", e.getMessage());
            } else {
                errors.rejectValue("email", "duplicate_email", e.getMessage());
            }
            return "employee-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditEmployeePage(@PathVariable Long id, Model model, HttpSession session) {

        try {
            authenticationHelper.verifySpecificAuthorization(session, EMPLOYEE_ROLE, (String) session.getAttribute("currentUserUsername"));
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }

        try {
            Employee employee = employeeService.getById(id);
            authenticationHelper.verifySpecificAuthorization(session, MANAGER_ROLE, employee.getUser().getUsername());
            EmployeeDto employeeDto = employeeModelMapper.toDto(employee);
            model.addAttribute("employee", employeeDto);
            return "";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }

    }

/*    @PostMapping("/update/{id}")
    public String updateEmployee(@Valid @ModelAttribute("employee") EmployeeDto employeeDto,
                                 @PathVariable Long id,
                                 BindingResult errors,
                                 Model model,
                                 HttpSession session) {
        if (errors.hasErrors()) {
            return "employee-new";
        }

        try {
            authenticationHelper.verifySpecificAuthorization(session, MANAGER_ROLE,(String)session.getAttribute("currentUserUsername"));
            Employee employee = employeeModelMapper.fromDto(employeeDto,id);
            employeeService.update(employee);
            return "redirect:/employees";
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("username")) {
                errors.rejectValue("username", "duplicate_username", e.getMessage());
            } else {
                errors.rejectValue("email", "duplicate_email", e.getMessage());
            }
            return "employee-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }
    }*/

    @GetMapping("/{id}/delete")
    public String deleteEmployee(@PathVariable Long id, Model model, HttpSession session) {
        try {
            authenticationHelper.verifyAuthorization(session, MANAGER_ROLE);
            employeeService.delete(id);
            return "redirect:/employees";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }


}
