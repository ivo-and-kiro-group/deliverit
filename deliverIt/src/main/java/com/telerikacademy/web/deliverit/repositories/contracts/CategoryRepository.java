package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.Category;

import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

public interface CategoryRepository {
    List<Category> getAll(Optional<String> name);

    Category getByName(String name);

    Category getById(Long id);

    Category create(Category category);

    Category update(Category category);

    Category delete(Category category);

}
