package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.Employee;
import com.telerikacademy.web.deliverit.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    List<User> getAll(Optional<String> firstName,
                          Optional<String> lastName);

    User getById(Long id);

    User getByEmail(String email);

    User getByUsername(String username);

    User create(User user);

    User update(User user);

    User delete(Long id);
}
