package com.telerikacademy.web.deliverit.services.modelMappers;

import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.ParcelDto;
import com.telerikacademy.web.deliverit.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ParcelModelMapper {
    private final ParcelService parcelService;
    private final CustomerService customerService;
    private final WarehouseService warehouseService;
    private final CategoryService categoryService;
    private final ShipmentService shipmentService;

    @Autowired
    public ParcelModelMapper(ParcelService parcelService, CustomerService customerService,
                             WarehouseService warehouseService, CategoryService categoryService, ShipmentService shipmentService){
        this.parcelService = parcelService;
        this.customerService = customerService;
        this.warehouseService = warehouseService;
        this.categoryService = categoryService;
        this.shipmentService = shipmentService;
    }

    public Parcel fromDto(ParcelDto parcelDto){
        Parcel parcel = new Parcel();
        dtoToObject(parcelDto, parcel);
        return parcel;
    }

    public Parcel fromDto(ParcelDto parcelDto,Long id){
        Parcel parcel = parcelService.getById(id);
        dtoToObject(parcelDto,parcel);
        return parcel;
    }

    public ParcelDto toDto(Parcel parcel){
        ParcelDto parcelDto = new ParcelDto();
        objectToDto(parcelDto,parcel);
        return parcelDto;
    }

    private void dtoToObject(ParcelDto parcelDto, Parcel parcel){

        parcel.setWarehouse(warehouseService.getById(parcelDto.getWarehouseId()));
        parcel.setCustomer(customerService.getById(parcelDto.getCustomerId()));
        parcel.setCategory(categoryService.getById(parcelDto.getCategoryId()));
        parcel.setShipment(shipmentService.getById(parcelDto.getShipmentId()));
        parcel.setWeight(parcelDto.getWeight());
        /*parcel.setCustomer(this.customerRepository.getById(parcelDto.getCustomerId()));
        parcel.setCategory(this.categoryRepository.getById(parcelDto.getCategoryId()));
        parcel.setWarehouse(this.warehouseRepository.getById(parcelDto.getWarehouseId()));*/

    }

    public void objectToDto(ParcelDto parcelDto,Parcel parcel){
        parcelDto.setWarehouseId(parcel.getWarehouse().getId());
        parcelDto.setCustomerId(parcel.getCustomer().getId());
        parcelDto.setCategoryId(parcel.getCategory().getId());
        parcelDto.setShipmentId(parcel.getShipment().getId());
        parcelDto.setWeight(parcelDto.getWeight());
    }

}
