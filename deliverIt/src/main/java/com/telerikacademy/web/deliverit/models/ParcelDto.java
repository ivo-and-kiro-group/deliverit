package com.telerikacademy.web.deliverit.models;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class ParcelDto {

    @NotNull(message = "Warehouse id should be not null.")
    @Positive(message = "Warehouse id should be positive.")
    private Long warehouseId;

    @NotNull(message = "Customer id should be not null.")
    @Positive(message = "Customer id should be positive.")
    private Long customerId;

    @NotNull(message = "Weight should be added!")
    @Positive(message = "Weight should be positive!")
    private Double weight;

    @NotNull(message = "Category id should be not null.")
    @Positive(message = "Category id should be positive.")
    private Long categoryId;

    @NotNull(message = "Shipment id should be not null.")
    @Positive(message = "Shipment id should be positive.")
    private Long shipmentId;

    public ParcelDto() {

    }

    public ParcelDto(Long warehouseId, Long customerId,
                     @Positive(message = "Weight should be positive!") Double weight,
                     Long categoryId,
                     Long shipmentId) {
        this.warehouseId = warehouseId;
        this.customerId = customerId;
        this.weight = weight;
        this.categoryId = categoryId;
        this.shipmentId = shipmentId;
    }

    public Long getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(Long shipmentId) {
        this.shipmentId = shipmentId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

}
