package com.telerikacademy.web.deliverit.controllers.rest;

import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Address;
import com.telerikacademy.web.deliverit.repositories.contracts.CityRepository;
import com.telerikacademy.web.deliverit.services.contracts.AddressService;
import com.telerikacademy.web.deliverit.services.modelMappers.AddressModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/addresses")
public class AddressController {

    private final AddressService service;

    @Autowired
    public AddressController(AddressService service, AddressModelMapper addressModelMapper, CityRepository cityRepository) {
        this.service = service;
    }

    @GetMapping
    public List<Address> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Address getById(@PathVariable Long id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

