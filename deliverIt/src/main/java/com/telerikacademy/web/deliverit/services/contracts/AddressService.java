package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.Address;
import com.telerikacademy.web.deliverit.models.City;

import java.util.List;
import java.util.Optional;

public interface AddressService {

    List<Address> getAll();

    Address getById(Long id);

    boolean getByStreetAndCity(String street, City city);

    Address create(Address address, City city);

    Address update(Address address, City city);

    void delete(Long id);


}
