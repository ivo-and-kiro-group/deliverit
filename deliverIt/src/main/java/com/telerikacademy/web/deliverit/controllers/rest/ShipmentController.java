package com.telerikacademy.web.deliverit.controllers.rest;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exceptions.DeletingEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exceptions.LocalDateException;
import com.telerikacademy.web.deliverit.models.*;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import com.telerikacademy.web.deliverit.services.contracts.ShipmentService;
import com.telerikacademy.web.deliverit.services.modelMappers.ShipmentModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("api/shipments")
public class ShipmentController {

    private final ShipmentService service;
    private final ParcelService parcelService;
    private final ShipmentModelMapper shipmentModelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ShipmentController(ShipmentService service,
                              ParcelService parcelService,
                              ShipmentModelMapper shipmentModelMapper,
                              AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.parcelService = parcelService;
        this.shipmentModelMapper = shipmentModelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Shipment> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetEmployee(headers);
        return service.getAll();
    }

  /*  @GetMapping
    public List<Shipment> filter(@RequestParam(required = false) Long warehouseId,
                                 @RequestParam(required = false) LocalDate departureDate,
                                 @RequestParam(required = false) LocalDate arrivalDate) {
        return service.filter(new ShipmentSearchParameters(departureDate, arrivalDate, warehouseId));
    }*/

    @GetMapping("/{id}")
    public Shipment getById(@RequestHeader HttpHeaders headers, @PathVariable Long id) {
        try {
            authenticationHelper.tryGetEmployee(headers);
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/parcel/{id}")
    public String getShipmentStatusByParcelId(@RequestHeader HttpHeaders headers, @PathVariable Long id) {
        try {
            Customer customer = authenticationHelper.tryGetCustomer(headers);
            Parcel parcel = parcelService.getById(id,customer);
            parcel.getShipment().calcStatus(parcelService.getShipmentParcels(parcel.getShipment().getId()));
            return parcel.getShipment().getStatus().toString();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/parcels")
    public List<Parcel> getShipmentParcels(@RequestHeader HttpHeaders headers, @PathVariable Long id) {
        try {
            authenticationHelper.tryGetEmployee(headers);
            return parcelService.getShipmentParcels(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/status")
    public String getShipmentStatus(@RequestHeader HttpHeaders headers,
                                    @PathVariable Long id) {
        try {
            Shipment shipment = getById(headers, id);
            service.getShipmentStatus(shipment);
            return shipment.getStatus().toString();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping()
    public Shipment create(@RequestHeader HttpHeaders headers, @Valid @RequestBody ShipmentDto shipmentDto) {
        try {
            authenticationHelper.tryGetEmployee(headers);
            Shipment shipment = shipmentModelMapper.fromDto(shipmentDto);
            return service.create(shipment);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (LocalDateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Shipment update(@RequestHeader HttpHeaders headers, @PathVariable Long id, @Valid @RequestBody ShipmentDto shipmentDto) {
        try {
            authenticationHelper.tryGetEmployee(headers);
            Shipment shipment = shipmentModelMapper.fromDto(shipmentDto, id);
            return service.update(shipment);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (LocalDateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable Long id) {
        try {
            authenticationHelper.tryGetEmployee(headers);
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DeletingEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}