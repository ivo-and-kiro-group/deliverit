package com.telerikacademy.web.deliverit.services.modelMappers;

import com.telerikacademy.web.deliverit.models.*;
import com.telerikacademy.web.deliverit.repositories.contracts.WarehouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WarehouseModelMapper {

    private final WarehouseRepository warehouseRepository;
    private final AddressModelMapper addressModelMapper;

    @Autowired
    public WarehouseModelMapper(WarehouseRepository warehouseRepository, AddressModelMapper addressModelMapper) {
        this.warehouseRepository = warehouseRepository;
        this.addressModelMapper = addressModelMapper;
    }

    public WarehouseSearchParameters fromDto(WarehouseSearchDto dto) {
        return new WarehouseSearchParameters(dto.getStreet(), dto.getCityId());
    }

    public Warehouse fromDto(WarehouseDto warehouseDto) {
        Warehouse warehouse = new Warehouse();
        dtoToObject(warehouseDto, warehouse);
        return warehouse;
    }

    public Warehouse fromDto(WarehouseDto warehouseDto, Long id) {
        Warehouse warehouse = warehouseRepository.getById(id);
        dtoToObjectUpdate(warehouseDto, warehouse);
        return warehouse;
    }

    public WarehouseDto toDto(Warehouse warehouse) {
        WarehouseDto warehouseDto = new WarehouseDto();
        AddressDto addressDto = addressModelMapper.toDto(warehouse.getAddress());
        warehouseDto.setAddress(addressDto);
        return warehouseDto;
    }

    private void dtoToObject(WarehouseDto warehouseDto, Warehouse warehouse) {
        Address address = addressModelMapper.fromDto(warehouseDto.getAddress());
        warehouse.setAddress(address);
    }

    private void dtoToObjectUpdate(WarehouseDto warehouseDto, Warehouse warehouse) {
        Address address = addressModelMapper.fromDto(warehouseDto.getAddress());
        warehouse.setAddress(address);
    }
}
