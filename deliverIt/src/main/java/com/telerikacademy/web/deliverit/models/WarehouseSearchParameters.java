package com.telerikacademy.web.deliverit.models;

public class WarehouseSearchParameters {
    private String street;
    private Long cityId;

    public WarehouseSearchParameters(String street, Long cityId) {
        this.street = street;
        this.cityId = cityId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }
}
