package com.telerikacademy.web.deliverit.repositories;

import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.repositories.contracts.CityRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class CityRepositoryImpl implements CityRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CityRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<City> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<City> query = session.createQuery("from City", City.class);
            return query.getResultList();
        }
    }

    @Override
    public City getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<City> query = session.createQuery("from City where name = :name", City.class);
            query.setParameter("name", name);

            List<City> result = query.list();

            if (result.size() == 0) {
                throw new EntityNotFoundException("City", "name", name);
            }
            return result.get(0);
        }
    }

    @Override
    public City getById(Long id) {
        try (Session session = sessionFactory.openSession()){
            City city = session.get(City.class, id);
            if (city == null) {
                throw new EntityNotFoundException("City", id);
            }
            return city;
        }
    }

}
