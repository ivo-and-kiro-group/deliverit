package com.telerikacademy.web.deliverit.controllers;

import com.telerikacademy.web.deliverit.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.Customer;
import com.telerikacademy.web.deliverit.models.Employee;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.repositories.contracts.UserRepository;
import com.telerikacademy.web.deliverit.services.contracts.CustomerService;
import com.telerikacademy.web.deliverit.services.contracts.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AuthenticationHelper {

    private static final String AUTHORIZATION_HEADER_USERNAME = "AuthorizationUsername";
    private static final String AUTHORIZATION_HEADER_PASSWORD = "AuthorizationPassword";

    private static final String AUTHORIZATION_RESOURCE_MESSAGE = "The request resource requires authorization.";
    private static final String INVALID_CREDENTIALS = "Invalid credentials";

    public static final String MANAGER_ROLE = "Manager";
    public static final String CUSTOMER_ROLE = "Customer";
    public static final String EMPLOYEE_ROLE = "Employee";

    private final EmployeeService employeeService;
    private final CustomerService customerService;
    private final UserRepository userRepository;

    @Autowired
    public AuthenticationHelper(EmployeeService employeeService, CustomerService customerService, UserRepository userRepository) {
        this.employeeService = employeeService;
        this.customerService = customerService;
        this.userRepository = userRepository;
    }


    public Employee tryGetEmployee(HttpHeaders headers) {
        if (!(headers.containsKey(AUTHORIZATION_HEADER_USERNAME) && headers.containsKey(AUTHORIZATION_HEADER_PASSWORD))) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, AUTHORIZATION_RESOURCE_MESSAGE);
        }

        try {
            String username = headers.getFirst(AUTHORIZATION_HEADER_USERNAME);
            String password = headers.getFirst(AUTHORIZATION_HEADER_PASSWORD);
            Employee employee = employeeService.getByUsername(username);
            if (employee.getUser().getPassword().equals(password)) {
                return employee;
            } else {
                throw new EntityNotFoundException("");
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_CREDENTIALS);
        }
    }

    public Employee tryGetEmployee(HttpHeaders headers, Long id) {
        if (!(headers.containsKey(AUTHORIZATION_HEADER_USERNAME) && headers.containsKey(AUTHORIZATION_HEADER_PASSWORD))) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, AUTHORIZATION_RESOURCE_MESSAGE);
        }

        try {
            String username = headers.getFirst(AUTHORIZATION_HEADER_USERNAME);
            String password = headers.getFirst(AUTHORIZATION_HEADER_PASSWORD);
            Employee employee = employeeService.getByUsername(username);
            if (employee.getUser().getPassword().equals(password) && employee.getId().equals(id)) {
                return employee;
            } else {
                throw new EntityNotFoundException("");
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_CREDENTIALS);
        }
    }

    public Employee tryGetEmployee(HttpHeaders headers, String emailOrUsername) {
        if (!(headers.containsKey(AUTHORIZATION_HEADER_USERNAME) && headers.containsKey(AUTHORIZATION_HEADER_PASSWORD))) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, AUTHORIZATION_RESOURCE_MESSAGE);
        }

        try {
            String username = headers.getFirst(AUTHORIZATION_HEADER_USERNAME);
            String password = headers.getFirst(AUTHORIZATION_HEADER_PASSWORD);
            Employee employee = employeeService.getByUsername(username);
            if (employee.getUser().getPassword().equals(password) && (employee.getUser().getUsername().equals(emailOrUsername) ||
                    employee.getUser().getEmail().equals(emailOrUsername))) {
                return employee;
            } else {
                throw new EntityNotFoundException("");
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_CREDENTIALS);
        }
    }

    public Employee tryGetManager(HttpHeaders headers) {

        if (!(headers.containsKey(AUTHORIZATION_HEADER_USERNAME) && headers.containsKey(AUTHORIZATION_HEADER_PASSWORD))) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, AUTHORIZATION_RESOURCE_MESSAGE);
        }

        try {
            String username = headers.getFirst(AUTHORIZATION_HEADER_USERNAME);
            String password = headers.getFirst(AUTHORIZATION_HEADER_PASSWORD);
            Employee employee = employeeService.getByUsername(username);
            if (employee.getUser().getPassword().equals(password) && employee.getUser().getRoles().stream().anyMatch(r -> r.getName().equals("Manager"))) {
                return employee;
            } else {
                throw new EntityNotFoundException("");
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_CREDENTIALS);
        }

    }

    public Employee tryGetManagerOrEmployee(HttpHeaders headers, Long id) {
        try {
            return tryGetEmployee(headers, id);
        } catch (ResponseStatusException r) {
            return tryGetManager(headers);
        }
    }

    public Customer tryGetCustomer(HttpHeaders headers) {
        if (!(headers.containsKey(AUTHORIZATION_HEADER_USERNAME) && headers.containsKey(AUTHORIZATION_HEADER_PASSWORD))) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, AUTHORIZATION_RESOURCE_MESSAGE);
        }

        try {
            String username = headers.getFirst(AUTHORIZATION_HEADER_USERNAME);
            String password = headers.getFirst(AUTHORIZATION_HEADER_PASSWORD);
            Customer customer = customerService.getByUsername(username);
            if (customer.getUser().getPassword().equals(password)) {
                return customer;
            } else {
                throw new EntityNotFoundException("");
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_CREDENTIALS);
        }
    }

    public Customer tryGetCustomer(HttpHeaders headers, Long id) {
        if (!(headers.containsKey(AUTHORIZATION_HEADER_USERNAME) && headers.containsKey(AUTHORIZATION_HEADER_PASSWORD))) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, AUTHORIZATION_RESOURCE_MESSAGE);
        }

        try {
            String username = headers.getFirst(AUTHORIZATION_HEADER_USERNAME);
            String password = headers.getFirst(AUTHORIZATION_HEADER_PASSWORD);
            Customer customer = customerService.getByUsername(username);
            if (customer.getUser().getPassword().equals(password) && customer.getId().equals(id)) {
                return customer;
            } else {
                throw new EntityNotFoundException("");
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid id.");
        }
    }

    public Customer tryGetCustomer(HttpHeaders headers, String emailOrUsername) {
        if (!(headers.containsKey(AUTHORIZATION_HEADER_USERNAME) && headers.containsKey(AUTHORIZATION_HEADER_PASSWORD))) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, AUTHORIZATION_RESOURCE_MESSAGE);
        }

        try {
            String username = headers.getFirst(AUTHORIZATION_HEADER_USERNAME);
            String password = headers.getFirst(AUTHORIZATION_HEADER_PASSWORD);
            Customer customer = customerService.getByUsername(username);
            if (customer.getUser().getPassword().equals(password) && (customer.getUser().getUsername().equals(emailOrUsername) ||
                    customer.getUser().getEmail().equals(emailOrUsername))) {
                return customer;
            } else {
                throw new EntityNotFoundException("");
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Can't see other user's personal data");
        }
    }

    // MVC Authentication
    public User tryGetUser(HttpSession session) {
        String currentUserUsername = (String) session.getAttribute("currentUserUsername");

        if (currentUserUsername == null) {
            throw new UnauthorizedOperationException("No logged in user.");
        }

        try {
            return userRepository.getByUsername(currentUserUsername);
        } catch (EntityNotFoundException e) {
            throw new UnauthorizedOperationException("No logged in user.");
        }
    }

    public User verifyAuthorization(HttpSession session, String role) {
        User user = tryGetUser(session);

        Set<String> userRoles = user
                .getRoles()
                .stream()
                .map(r -> r.getName().toLowerCase())
                .collect(Collectors.toSet());

        if (!userRoles.contains(role.toLowerCase())) {
            throw new UnauthorizedOperationException("User does not have the required authorization.");
        }

        return user;
    }

    public User verifySpecificAuthorization(HttpSession session, String role,String username) {
        User user = verifyAuthorization(session,role);
        if (user.getUsername().equals(username)) {
            throw new UnauthorizedOperationException("User does not have the required authorization.");
        }
        return user;
    }

    public User verifyAuthentication(String username, String password) {
        try {
            User user = userRepository.getByUsername(username);

            if (!user.getPassword().equals(password)) {
                throw new AuthenticationFailureException("Wrong username/password.");
            }

            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException("Wrong username/password.");
        }
    }

}
