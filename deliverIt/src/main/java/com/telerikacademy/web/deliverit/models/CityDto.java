package com.telerikacademy.web.deliverit.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class CityDto {

    @NotEmpty
    @Size(min = 2, max = 20, message = "name should be between 2 and 20 symbols!")
    private String name;

    @NotEmpty
    @Size(min=5,max = 20, message = "Post code should be between 5 and 20 symbols.")
    private String postCode;

    @NotEmpty
    private Long countryId;

    public CityDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }
}
