package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exceptions.DeletingEntityException;
import com.telerikacademy.web.deliverit.exceptions.LocalDateException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.enums.Status;
import com.telerikacademy.web.deliverit.repositories.contracts.ShipmentRepository;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.web.deliverit.helpers.ShipmentHelper.createMockShipment;
import static com.telerikacademy.web.deliverit.helpers.ShipmentHelper.getParcelsToShipment;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ShipmentServiceImplTests {
    @Mock
    ShipmentRepository mockRepository;

    @Mock
    ParcelService parcelService;

    @InjectMocks
    ShipmentServiceImpl service;

    @Test
    public void getAll_Should_Call_Repository() {

        when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        verify(mockRepository, times(1)).getAll();
    }


    @Test
    public void getById_Should_Call_Repository_When_ShipmentExist() {

        Mockito.when(mockRepository.getById(1L))
                .thenReturn(createMockShipment());
        // Act;
        Shipment shipment = service.getById(1L);
        // Assert
        Assertions.assertEquals(1, shipment.getId());
        Assertions.assertEquals(LocalDate.of(2021, 03, 10), shipment.getDepartureDate());
        Assertions.assertEquals(LocalDate.of(2021, 03, 20), shipment.getArrivalDate());
    }

    @Test
    public void getShipmentParcels_Should_Call_Repository_When_ShipmentExist() {

        List<Parcel> parcels = getParcelsToShipment();
        Mockito.when(parcelService.getShipmentParcels(1L)).thenReturn(parcels);

        Assertions.assertEquals(parcels, service.getShipmentParcels(1L));
    }

    @Test
    public void create_Should_Throw_Exception_When_ShipmentDepartureDateInvalid() {

        Shipment shipment = createMockShipment();
        shipment.setDepartureDate(LocalDate.of(2020,5,20));

        Assertions.assertThrows(LocalDateException.class,()-> service.create(shipment));
    }

    @Test
    public void create_Should_Throw_Exception_When_ShipmentArrivalDateInvalid() {

        Shipment shipment = createMockShipment();
        shipment.setArrivalDate(LocalDate.of(2020,5,20));

        Assertions.assertThrows(LocalDateException.class,()-> service.create(shipment));
    }

    @Test
    public void create_Should_Throw_Exception_When_ShipmentArrivalDateIsBeforeDepartureDate() {

        Shipment shipment = createMockShipment();
        shipment.setDepartureDate(LocalDate.of(2021,6,20));
        shipment.setArrivalDate(LocalDate.of(2021,5,20));

        Assertions.assertThrows(LocalDateException.class,()-> service.create(shipment));
    }

    @Test
    public void update_Should_Throw_When_ShipmentStatusOnTheWay() {

        Shipment shipment = createMockShipment();
        shipment.setDepartureDate(LocalDate.of(2021,2,20));
        List<Parcel> parcels = getParcelsToShipment();

        Mockito.when(service.getById(1L)).thenReturn(shipment);
        Mockito.when(parcelService.getShipmentParcels(1L)).thenReturn(parcels);

        Assertions.assertThrows(LocalDateException.class,()-> service.update(shipment));
    }


    @Test
    public void update_Should_Throw_When_ShipmentStatusCompleted() {

        Shipment shipment = createMockShipment();
        shipment.setArrivalDate(LocalDate.of(2021,2,10));
        shipment.setArrivalDate(LocalDate.of(2021,2,20));
        List<Parcel> parcels = getParcelsToShipment();

        Mockito.when(service.getById(1L)).thenReturn(shipment);
        Mockito.when(parcelService.getShipmentParcels(1L)).thenReturn(parcels);

        Assertions.assertThrows(LocalDateException.class,()-> service.update(shipment));
    }

    @Test
    public void update_Should_Throw_Exception_When_ShipmentArrivalDateIsBeforeDepartureDate() {

        Shipment shipment = createMockShipment();
        List<Parcel> parcels = getParcelsToShipment();

        Mockito.when(service.getById(1L)).thenReturn(shipment);
        Mockito.when(parcelService.getShipmentParcels(1L)).thenReturn(parcels);

        shipment.setDepartureDate(LocalDate.of(2021,6,20));
        shipment.setArrivalDate(LocalDate.of(2021,5,20));

        Assertions.assertThrows(LocalDateException.class,()-> service.update(shipment));
    }

    @Test
    public void update_Should_Work_When_ShipmentArrivalDateIsBeforeDepartureDate() {

        Shipment shipment = createMockShipment();
        List<Parcel> parcels = getParcelsToShipment();

        Mockito.when(service.getById(1L)).thenReturn(shipment);
        Mockito.when(parcelService.getShipmentParcels(1L)).thenReturn(new ArrayList<>());
        Mockito.when(mockRepository.update(shipment)).thenReturn(shipment);

        service.update(shipment);

        verify(mockRepository,times(1)).update(shipment);
    }

    @Test
    public void create_Should_Work_When_ShipmentArrivalDateIsBeforeDepartureDate() {
        Shipment shipment = createMockShipment();
        List<Parcel> parcels = getParcelsToShipment();

        service.create(shipment);

        verify(mockRepository,times(1)).create(shipment);
    }

    @Test
    public void getShipmentStatus_Call_CheckStatus() {

        Shipment shipment = createMockShipment();
        List<Parcel> parcels = getParcelsToShipment();

        Mockito.when(parcelService.getShipmentParcels(shipment.getId())).thenReturn(parcels);

        service.getShipmentStatus(shipment);

        verify(parcelService,times(1)).getShipmentParcels(shipment.getId());

        Assertions.assertEquals(Status.ONTHEWAY,shipment.getStatus());
    }

    @Test
    public void delete_Shoul_Throw_Exception_When_StatusOnTheWay() {

        Shipment shipment = createMockShipment();
        shipment.setDepartureDate(LocalDate.of(2021,2,20));
        List<Parcel> parcels = getParcelsToShipment();

        Mockito.when(service.getById(1L)).thenReturn(shipment);
        Mockito.when(parcelService.getShipmentParcels(shipment.getId())).thenReturn(parcels);



        Assertions.assertThrows(DeletingEntityException.class,()->  service.delete(shipment.getId()));
    }

    @Test
    public void delete_Shoul_Throw_Exception_When_ParcelsExists() {

        Shipment shipment = createMockShipment();
        List<Parcel> parcels = getParcelsToShipment();

        Mockito.when(service.getById(1L)).thenReturn(shipment);
        Mockito.when(parcelService.getShipmentParcels(shipment.getId())).thenReturn(parcels);

        Assertions.assertThrows(DeletingEntityException.class,()->  service.delete(shipment.getId()));
    }
}
