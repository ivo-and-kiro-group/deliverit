package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.helpers.ParcelHelper;
import com.telerikacademy.web.deliverit.models.Customer;
import com.telerikacademy.web.deliverit.models.Employee;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.repositories.contracts.CustomerRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.ParcelRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.telerikacademy.web.deliverit.helpers.CustomersHelper.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceImplTests {

    @Mock
    CustomerRepository mockRepository;

    @InjectMocks
    CustomerServiceImpl service;

    @Test
    public void getAll_Should_Call_Repository() {

        when(mockRepository.getAll(Optional.empty(),
                Optional.empty(),
                Optional.empty()))
                .thenReturn(new ArrayList<Customer>());

        service.getAll(Optional.empty(),
                Optional.empty(),
                Optional.empty());

        verify(mockRepository, times(1)).getAll(Optional.empty(),
                Optional.empty(),
                Optional.empty());
    }

    @Test
    public void getById_Should_Call_Repository_When_CustomerExist() {

        Mockito.when(mockRepository.getById(1L))
                .thenReturn(createMockCustomer());
        // Act
        Customer result = service.getById(1L);

        // Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("MockUsername", result.getUser().getUsername());
        Assertions.assertEquals("mock@email", result.getUser().getEmail());
        Assertions.assertEquals("MockPassword", result.getUser().getPassword());
        Assertions.assertEquals("MockFirstName", result.getUser().getFirstName());
        Assertions.assertEquals("MockLastName", result.getUser().getLastName());
        Assertions.assertEquals("customerMockStreet",result.getAddress().getStreet());
        Assertions.assertEquals(1L,result.getAddress().getCity().getId());
    }

    @Test
    public void getByUsername_Should_Call_Repository_When_CustomerExist() {

        Mockito.when(mockRepository.getByUsername("MockUsername"))
                .thenReturn(createMockCustomer());
        // Act
        Customer result = service.getByUsername("MockUsername");

        // Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("MockUsername", result.getUser().getUsername());
        Assertions.assertEquals("mock@email", result.getUser().getEmail());
        Assertions.assertEquals("MockPassword", result.getUser().getPassword());
        Assertions.assertEquals("MockFirstName", result.getUser().getFirstName());
        Assertions.assertEquals("MockLastName", result.getUser().getLastName());
        Assertions.assertEquals("customerMockStreet",result.getAddress().getStreet());
        Assertions.assertEquals(1L,result.getAddress().getCity().getId());
    }

    @Test
    public void getByEmail_Should_Call_Repository_When_CustomerExist() {

        Mockito.when(mockRepository.getByEmail("mock@email"))
                .thenReturn(createMockCustomer());
        // Act
        Customer result = service.getByEmail("mock@email");

        // Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("MockUsername", result.getUser().getUsername());
        Assertions.assertEquals("mock@email", result.getUser().getEmail());
        Assertions.assertEquals("MockPassword", result.getUser().getPassword());
        Assertions.assertEquals("MockFirstName", result.getUser().getFirstName());
        Assertions.assertEquals("MockLastName", result.getUser().getLastName());
        Assertions.assertEquals("customerMockStreet",result.getAddress().getStreet());
        Assertions.assertEquals(1L,result.getAddress().getCity().getId());
    }

    @Test
    public void getByAddress_Should_Call_Repository_When_CustomerExist() {

        Mockito.when(mockRepository.getByAddress(createMockAddress()))
                .thenReturn(createMockCustomer());
        // Act
        Customer result = service.getByAddress(createMockAddress());

        // Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("MockUsername", result.getUser().getUsername());
        Assertions.assertEquals("mock@email", result.getUser().getEmail());
        Assertions.assertEquals("MockPassword", result.getUser().getPassword());
        Assertions.assertEquals("MockFirstName", result.getUser().getFirstName());
        Assertions.assertEquals("MockLastName", result.getUser().getLastName());
        Assertions.assertEquals("customerMockStreet",result.getAddress().getStreet());
        Assertions.assertEquals(1L,result.getAddress().getCity().getId());
    }

    @Test
    public void create_Should_Throw_Duplicate_When_EmailExist() {

        // Arrange
        Customer customer = createMockCustomer();

       Mockito.when(mockRepository.getByEmail(customer.getUser().getEmail()))
                .thenReturn(customer);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(customer));
    }

    @Test
    public void create_Should_Throw_Duplicate_When_UsernameExist() {

        // Arrange
        Customer customer = createMockCustomer();

        Mockito.when(mockRepository.getByEmail(customer.getUser().getEmail()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.getByUsername(customer.getUser().getUsername()))
                .thenReturn(customer);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(customer));
    }

    @Test
    public void create_Should_Throw_Duplicate_When_AddressExists() {

        // Arrange
        Customer customer = createMockCustomer();

        Mockito.when(mockRepository.getByEmail(customer.getUser().getEmail()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.getByUsername(customer.getUser().getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.getByAddress(customer.getAddress()))
                .thenReturn(customer);
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(customer));
    }

    @Test
    public void update_Should_Throw_Duplicate_When_EmailExist() {

        // Arrange
        Customer customer = createMockCustomer();
        customer.setId(2L);

        Mockito.when(mockRepository.getByEmail(customer.getUser().getEmail()))
                .thenReturn(createMockCustomer());

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(customer));
    }

    @Test
    public void update_Should_Throw_Duplicate_When_UsernameExist() {

        // Arrange
        Customer customer = createMockCustomer();
        customer.setId(2L);

        Mockito.when(mockRepository.getByEmail(customer.getUser().getEmail()))
                .thenReturn(customer);

        Mockito.when(mockRepository.getByUsername(customer.getUser().getUsername()))
                .thenReturn(createMockCustomer());

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(customer));
    }

    @Test
    public void update_Should_Throw_Duplicate_When_AddressExists() {

        // Arrange
        Customer customer = createMockCustomer();
        customer.setId(2L);

        Mockito.when(mockRepository.getByEmail(customer.getUser().getEmail()))
                .thenReturn(customer);

        Mockito.when(mockRepository.getByUsername(customer.getUser().getUsername()))
                .thenReturn(customer);

        Mockito.when(mockRepository.getByAddress(customer.getAddress()))
                .thenReturn(createMockCustomer());
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(customer));
    }

    @Test
    public void delete_Should_Throw_Duplicate_When_AddressExists() {
        // Arrange
        Customer customer = createMockCustomer();

        customer.setId(2L);

        Mockito.when(mockRepository.delete(Mockito.anyLong()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> service.delete(Mockito.anyLong()));
    }

}
