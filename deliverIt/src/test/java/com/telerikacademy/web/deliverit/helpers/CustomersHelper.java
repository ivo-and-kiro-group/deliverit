package com.telerikacademy.web.deliverit.helpers;

import com.telerikacademy.web.deliverit.models.*;
import com.telerikacademy.web.deliverit.models.Address;

import java.util.HashSet;
import java.util.Set;

public class CustomersHelper {

    public static Customer createMockCustomer() {
        var mockCustomer = new Customer();
        mockCustomer.setId(1L);
        mockCustomer.setUser(createMockUser());
        mockCustomer.setAddress(createMockAddress());
        return mockCustomer;
    }

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setId(1L);
        mockUser.setEmail("mock@email");
        mockUser.setUsername("MockUsername");
        mockUser.setPassword("MockPassword");
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setRoles(Set.of(createMockRole()));
        return mockUser;
    }

    public static Role createMockRole() {
        var mockRole = new Role();
        mockRole.setName("Customer");
        mockRole.setId(1L);
        return mockRole;
    }

    public static Address createMockAddress() {
        var mockAddress = new Address();
        mockAddress.setId(1L);
        mockAddress.setCity(createMockCity());
        mockAddress.setStreet("customerMockStreet");
        return mockAddress;
    }

    public static City createMockCity() {
        var mockCity = new City();
        mockCity.setId(1L);
        mockCity.setName("customerMockCity");
        mockCity.setCountry(createMockCountry());
        mockCity.setPostCode("112233");
        return mockCity;
    }

    public static Country createMockCountry() {
        var mockCountry = new Country();
        mockCountry.setId(1L);
        mockCountry.setName("customerMockCountry");
        return mockCountry;
    }
}
