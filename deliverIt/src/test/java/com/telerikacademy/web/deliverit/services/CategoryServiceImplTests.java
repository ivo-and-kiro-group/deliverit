package com.telerikacademy.web.deliverit.services;


import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Category;
import com.telerikacademy.web.deliverit.models.Customer;
import com.telerikacademy.web.deliverit.models.Employee;
import com.telerikacademy.web.deliverit.repositories.contracts.CategoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.telerikacademy.web.deliverit.helpers.CustomersHelper.createMockCustomer;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class CategoryServiceImplTests {
    @Mock
    CategoryRepository mockRepository;

    @InjectMocks
    CategoryServiceImpl service;

    private Category category;

    @BeforeEach
    public void setInstance(){
        category = new Category();
        category.setId(1L);
        category.setName("Medical");
    }


    @Test
    public void getAll_Should_Call_Repository() {

        when(mockRepository.getAll(Optional.empty()))
                .thenReturn(new ArrayList<>());

        service.getAll(Optional.empty());

        verify(mockRepository, times(1)).getAll(Optional.empty());
    }

    @Test
    public void getById_Should_Call_Repository_When_CategoryExist() {

        Mockito.when(mockRepository.getById(1L))
                .thenReturn(category);
        // Act
        Category result = service.getById(1L);

        // Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("Medical", result.getName());
    }

    @Test
    public void create_Should_Call_Throw_Exception_When_CategoryExist() {

        Mockito.when(mockRepository.getByName(category.getName()))
                .thenReturn(category);

        // Assert
        Assertions.assertThrows(DuplicateEntityException.class, ()-> service.create(category));
    }

    @Test
    public void update_Should_Call_Throw_Exception_When_CategoryExist() {

        Category category2 = new Category();
        category2.setName("Medical");
        category2.setId(3L);
        Mockito.when(mockRepository.getByName(category.getName()))
                .thenReturn(category);

        // Assert
        Assertions.assertThrows(DuplicateEntityException.class, ()-> service.update(category2));
    }

    @Test
    public void update_Should_Call_Work_When_CategoryDoesNotExist() {

        Category category2 = new Category();
        category2.setName("Medical");
        category2.setId(3L);
        Mockito.when(mockRepository.getByName(category.getName()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.update(category))
                .thenReturn(category);

        // Assert
        Assertions.assertEquals(category,service.update(category));
    }

    @Test
    public void delete_Should_Call_Throw_Exception_When_CategoryDoesNotExist() {

        Category category2 = new Category();

        Mockito.when(service.getById(Mockito.anyLong())).thenThrow(EntityNotFoundException.class);

        // Assert
        Assertions.assertThrows(EntityNotFoundException.class, ()-> service.delete(Mockito.anyLong()));
    }

    @Test
    public void delete_Should_Call_Work_When_CategoryExist() {

        Category category2 = new Category();

        Mockito.when(service.getById(Mockito.anyLong())).thenReturn(category2);

        Mockito.when(mockRepository.delete(category2)).thenReturn(category2);

        // Assert
        Assertions.assertEquals(category2, service.delete(Mockito.anyLong()));
    }
}
