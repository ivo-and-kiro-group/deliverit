package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.helpers.AddressHelper;
import com.telerikacademy.web.deliverit.helpers.CustomersHelper;
import com.telerikacademy.web.deliverit.helpers.WarehouseHelper;
import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Address;
import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.models.Country;
import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.repositories.contracts.CustomerRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.WarehouseRepository;
import com.telerikacademy.web.deliverit.services.contracts.CustomerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class WarehouseServiceImplTests {
    @Mock
    WarehouseRepository mockRepository;

    @Mock
    CustomerServiceImpl customerService;

    @InjectMocks
    WarehouseServiceImpl warehouseService;


    @Test
    public void getById_Should_ReturnWarehouse_When_MatchExist() {
        // Arrange
        Mockito.when(mockRepository.getById(2L))
                .thenReturn(new Warehouse(2L,
                        new Address(1L, "test",
                                new City(1L, "City",
                                        new Country(1L, "Country"),
                                        "123"))));
        // Act
        Warehouse result = warehouseService.getById(2L);

        // Assert
        Assertions.assertEquals(2, result.getId());
        Assertions.assertEquals("test", result.getAddress().getStreet());
        Assertions.assertEquals(1, result.getAddress().getCity().getId());
        Assertions.assertEquals("City", result.getAddress().getCity().getName());
        Assertions.assertEquals(1, result.getAddress().getCity().getCountry().getId());
        Assertions.assertEquals(1, result.getAddress().getCity().getCountry().getId());
        Assertions.assertEquals("123", result.getAddress().getCity().getPostCode());
    }

    @Test
    public void getById_Should_Throw_When_WarehouseDoesntExist() {
        var mockWarehouse = WarehouseHelper.createMockWarehouse();

        when(mockRepository.getById(mockWarehouse.getId()))
                .thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> warehouseService.getById(mockWarehouse.getId()));
    }

    @Test
    public void getAll_Should_Call_Repository() {

        when(mockRepository.getAll())
                .thenReturn(new ArrayList<Warehouse>());

        warehouseService.getAll();

        verify(mockRepository, times(1)).getAll();
    }


    @Test
    public void create_Should_Throw_When_WarehouseWithSameAddressExists() {
        // Arrange
        var mockWarehouse = WarehouseHelper.createMockWarehouse();

        Mockito.when(mockRepository.getByStreetAndCity(mockWarehouse.getAddress().getStreet(),
                mockWarehouse.getAddress().getCity()))
                .thenReturn(mockWarehouse);

        // Assert & Act
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> warehouseService.create(mockWarehouse));
    }


    @Test
    public void create_Should_Throw_When_CustomerAddressWithSameAddressExists() {
        // Arrange
        var mockWarehouse = WarehouseHelper.createMockWarehouse();
        var mockCustomer = CustomersHelper.createMockCustomer();

        Mockito.when(mockRepository.getByStreetAndCity(mockWarehouse.getAddress().getStreet(),
                mockWarehouse.getAddress().getCity()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(customerService.getByAddress(mockWarehouse.getAddress()))
                .thenReturn(mockCustomer);

        // Assert & Act
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> warehouseService.create(mockWarehouse));
    }

    @Test
    public void create_should_CallRepository_When_AddressDoesntExist() {
        var mockWarehouse = WarehouseHelper.createMockWarehouse();

        when(mockRepository.getByStreetAndCity(mockWarehouse.getAddress().getStreet(),
                mockWarehouse.getAddress().getCity()))
                .thenThrow(EntityNotFoundException.class);

        when(customerService.getByAddress(mockWarehouse.getAddress()))
                .thenThrow(EntityNotFoundException.class);

        warehouseService.create(mockWarehouse);

        verify(mockRepository, times(1)).create(mockWarehouse);

    }


    @Test
    public void create_Should_CallRepository_When_WarehouseWithSameAddressDoesNotExistForWarehouses() {
        // Arrange
        var mockWarehouse = WarehouseHelper.createMockWarehouse();

        Mockito.when(mockRepository.getByStreetAndCity(mockWarehouse.getAddress().getStreet(),
                mockWarehouse.getAddress().getCity()))
                .thenThrow(EntityNotFoundException.class);


        Mockito.when(customerService.getByAddress(mockWarehouse.getAddress()))
                .thenThrow(EntityNotFoundException.class);


        warehouseService.create(mockWarehouse);

        // Act, Assert
        Mockito.verify(mockRepository, times(1))
                .create(mockWarehouse);
    }

    @Test
    public void update_Should_Call_Repository_When_AddressExists() {
        // Arrange
        var mockWarehouse = WarehouseHelper.createMockWarehouse();

        when(mockRepository.getByStreetAndCity(mockWarehouse.getAddress().getStreet(),
                mockWarehouse.getAddress().getCity()))
                .thenReturn(mockWarehouse);

        when(mockRepository.getByStreetAndCity(mockWarehouse.getAddress().getStreet(),
                mockWarehouse.getAddress().getCity()))
                .thenThrow(EntityNotFoundException.class);

        when(customerService.getByAddress(mockWarehouse.getAddress()))
                .thenThrow(EntityNotFoundException.class);

        warehouseService.update(mockWarehouse);

        // Act, Assert
        verify(mockRepository, times(1)).update(mockWarehouse);
    }

    @Test
    public void update_Should_Throw_When_CustomerHasSameAddress() {
        var mockWarehouse = WarehouseHelper.createMockWarehouse();
        var mockCustomer = CustomersHelper.createMockCustomer();

        when(mockRepository.getByStreetAndCity(mockWarehouse.getAddress().getStreet(),
                mockWarehouse.getAddress().getCity()))
                .thenThrow(EntityNotFoundException.class);

        when(customerService.getByAddress(mockWarehouse.getAddress()))
                .thenReturn(mockCustomer);

        assertThrows(DuplicateEntityException.class,
                () -> warehouseService.update(mockWarehouse));
    }

    @Test
    public void update_Should_Throw_When_AddressExist() {
        var mockWarehouse = WarehouseHelper.createMockWarehouse();
        var anotherWarehouse = WarehouseHelper.createMockWarehouse();
        anotherWarehouse.setId(2L);


        when(mockRepository.getByStreetAndCity(mockWarehouse.getAddress().getStreet(),
                mockWarehouse.getAddress().getCity()))
                .thenReturn(anotherWarehouse);

        assertThrows(DuplicateEntityException.class,
                () -> warehouseService.update(mockWarehouse));
    }

    @Test
    public void delete_Should_Call_Repository_When_Warehouse_Exists() {

        warehouseService.delete(1L);

        verify(mockRepository, times(1)).delete(1L);
    }

    @Test
    public void delete_Should_Throw_When_Warehouse_DoesntExists() {

        when(mockRepository.delete(1L)).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> warehouseService.delete(1L));
    }


}
