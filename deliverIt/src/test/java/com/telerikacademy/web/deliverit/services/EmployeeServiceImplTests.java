package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Employee;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.repositories.contracts.EmployeeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.telerikacademy.web.deliverit.helpers.EmployeeHelper.createMockEmployee;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class EmployeeServiceImplTests {

    @Mock
    EmployeeRepository mockRepository;

    @InjectMocks
    EmployeeServiceImpl service;

    @Test
    public void getAll_Should_Call_Repository() {

        when(mockRepository.getAll(Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty()))
                .thenReturn(new ArrayList<Employee>());

        service.getAll(Optional.empty(),
                Optional.empty(),
                Optional.empty()
        ,Optional.empty());

        verify(mockRepository, times(1)).getAll(Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());
    }

    @Test
    public void getById_Should_Call_Repository_When_EmployeeExist() {

        Mockito.when(mockRepository.getById(1L))
                .thenReturn(createMockEmployee());
        // Act
        Employee result = service.getById(1L);

        // Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("MockUsername", result.getUser().getUsername());
        Assertions.assertEquals("mock@email", result.getUser().getEmail());
        Assertions.assertEquals("MockPassword", result.getUser().getPassword());
        Assertions.assertEquals("MockFirstName", result.getUser().getFirstName());
        Assertions.assertEquals("MockLastName", result.getUser().getLastName());
    }

    @Test
    public void getByUsername_Should_Call_Repository_When_EmployeeExist() {

        Mockito.when(mockRepository.getByUsername("MockUsername"))
                .thenReturn(createMockEmployee());
        // Act
        Employee result = service.getByUsername("MockUsername");

        // Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("MockUsername", result.getUser().getUsername());
        Assertions.assertEquals("mock@email", result.getUser().getEmail());
        Assertions.assertEquals("MockPassword", result.getUser().getPassword());
        Assertions.assertEquals("MockFirstName", result.getUser().getFirstName());
        Assertions.assertEquals("MockLastName", result.getUser().getLastName());
    }

    @Test
    public void getByEmail_Should_Call_Repository_When_EmployeeExist() {

        Mockito.when(mockRepository.getByEmail("mock@email"))
                .thenReturn(createMockEmployee());
        // Act
        Employee result = service.getByEmail("mock@email");

        // Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("MockUsername", result.getUser().getUsername());
        Assertions.assertEquals("mock@email", result.getUser().getEmail());
        Assertions.assertEquals("MockPassword", result.getUser().getPassword());
        Assertions.assertEquals("MockFirstName", result.getUser().getFirstName());
        Assertions.assertEquals("MockLastName", result.getUser().getLastName());
        
    }

    @Test
    public void create_Should_Throw_Duplicate_When_EmailExist() {

        // Arrange
        Employee employee  = createMockEmployee();

        Mockito.when(mockRepository.getByEmail(employee .getUser().getEmail()))
                .thenReturn(employee );

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(employee ));
    }

    @Test
    public void create_Should_Throw_Duplicate_When_UsernameExist() {

        // Arrange
        Employee employee  = createMockEmployee();

        Mockito.when(mockRepository.getByEmail(employee .getUser().getEmail()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.getByUsername(employee .getUser().getUsername()))
                .thenReturn(employee );

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(employee ));
    }

    @Test
    public void update_Should_Throw_Duplicate_When_EmailExist() {

        // Arrange
        Employee employee  = createMockEmployee();
        employee .setId(2L);

        Mockito.when(mockRepository.getByEmail(employee .getUser().getEmail()))
                .thenReturn(createMockEmployee());

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(employee ));
    }

    @Test
    public void update_Should_Throw_Duplicate_When_UsernameExist() {

        // Arrange
        Employee employee  = createMockEmployee();
        employee .setId(2L);

        Mockito.when(mockRepository.getByEmail(employee .getUser().getEmail()))
                .thenReturn(employee );

        Mockito.when(mockRepository.getByUsername(employee .getUser().getUsername()))
                .thenReturn(createMockEmployee());

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(employee ));
    }

    @Test
    public void delete_Should_Throw_Duplicate_When_EmployeeDoesntExist() {
        // Arrange
        Employee employee  = createMockEmployee();

        employee .setId(2L);

        Mockito.when(mockRepository.delete(Mockito.anyLong()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> service.delete(Mockito.anyLong()));
    }
}