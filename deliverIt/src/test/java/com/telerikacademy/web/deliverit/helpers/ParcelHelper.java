package com.telerikacademy.web.deliverit.helpers;

import com.telerikacademy.web.deliverit.models.Category;
import com.telerikacademy.web.deliverit.models.Parcel;

public class ParcelHelper {

    public static Parcel createMockParcel() {
        var mockParcel = new Parcel();
        mockParcel.setId(1L);
        mockParcel.setWeight(2.5);
        mockParcel.setCustomer(CustomersHelper.createMockCustomer());
        mockParcel.setShipment(ShipmentHelper.createMockShipment());
        mockParcel.setCategory(createMockCategory());
        mockParcel.setWarehouse(WarehouseHelper.createMockWarehouse());
        return mockParcel;
    }

    public static Category createMockCategory() {
        var mockCategory = new Category();
        mockCategory.setId(1L);
        mockCategory.setName("mockCategory");
        return mockCategory;
    }
}
