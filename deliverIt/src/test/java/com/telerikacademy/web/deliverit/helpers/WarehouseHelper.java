package com.telerikacademy.web.deliverit.helpers;

import com.telerikacademy.web.deliverit.models.Address;
import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.models.Country;
import com.telerikacademy.web.deliverit.models.Warehouse;

public class WarehouseHelper {

    public static Warehouse createMockWarehouse() {
        var mockWarehouse = new Warehouse();
        mockWarehouse.setId(1L);
        mockWarehouse.setAddress(createMockAddress());

        return mockWarehouse;
    }

    public static Address createMockAddress() {
        var mockAddress = new Address();
        mockAddress.setId(1L);
        mockAddress.setCity(createMockCity());
        mockAddress.setStreet("mockWarehouseStreet");
        return mockAddress;
    }

    public static City createMockCity() {
        var mockCity = new City();
        mockCity.setId(1L);
        mockCity.setName("mockWarehouseCity");
        mockCity.setCountry(createMockCountry());
        mockCity.setPostCode("112233");
        return mockCity;
    }

    public static Country createMockCountry() {
        var mockCountry = new Country();
        mockCountry.setId(1L);
        mockCountry.setName("mockWarehouseCountry");
        return mockCountry;
    }
}
