package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.helpers.AddressHelper;
import com.telerikacademy.web.deliverit.models.Address;
import com.telerikacademy.web.deliverit.repositories.contracts.AddressRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AddressServiceImplTests {
    @Mock
    AddressRepository repository;

    @InjectMocks
    AddressServiceImpl service;

    @Test
    public void getById_Should_Call_Repository() {
        when(repository.getById(1L))
                .thenReturn(any(Address.class));

        service.getById(1L);

        verify(repository, times(1)).getById(1L);
    }

    @Test
    public void getById_Should_Throw_When_AddressDoesntExists() {
        when(repository.getById(1L))
                .thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getById(1L));
    }

    @Test
    public void getAll_Should_Call_Repository() {
        when(repository.getAll())
                .thenReturn(new ArrayList<Address>());

        service.getAll();

        verify(repository, times(1)).getAll();
    }

    @Test
    public void getByCityAndStreet_Should_Call_RepositoryWhenAddressDoesntExist() {
        var mockAddress = AddressHelper.createMockAddress();

        when(repository.getByCityAndStreet(mockAddress.getStreet(),
                mockAddress.getCity()))
                .thenReturn(true);

        service.getByStreetAndCity(mockAddress.getStreet(),
                mockAddress.getCity());

        verify(repository, times(1))
                .getByCityAndStreet(mockAddress.getStreet(), mockAddress.getCity());
    }

    @Test
    public void create_Should_Call_Repository_When_AddressDoesntExist() {
        var mockAddress = AddressHelper.createMockAddress();

        when(repository.getByCityAndStreet(mockAddress.getStreet(),
                mockAddress.getCity())).thenReturn(false);

        service.create(mockAddress, mockAddress.getCity());

        verify(repository, times(1)).create(mockAddress);
    }

    @Test
    public void create_Should_Throw_When_AddressExist() {
        var mockAddress = AddressHelper.createMockAddress();

        when(repository.getByCityAndStreet(mockAddress.getStreet(),
                mockAddress.getCity())).thenReturn(true);

        assertThrows(DuplicateEntityException.class,
                () -> service.create(mockAddress, mockAddress.getCity()));
    }

    @Test
    public void update_Should_Call_Repository_When_AddressDoesntExist() {
        var mockAddress = AddressHelper.createMockAddress();

        when(repository.getByCityAndStreet(mockAddress.getStreet(),
                mockAddress.getCity()))
                .thenReturn(false);

        service.update(mockAddress, mockAddress.getCity());

        verify(repository, times(1)).update(mockAddress);
    }

    @Test
    public void update_Should_Throw_When_AddressExist() {
        var mockAddress = AddressHelper.createMockAddress();

        when(repository.getByCityAndStreet(mockAddress.getStreet(),
                mockAddress.getCity())).thenReturn(true);

        assertThrows(DuplicateEntityException.class,
                () -> service.update(mockAddress, mockAddress.getCity()));
    }

    @Test
    public void delete_Should_Call_Repository_When_AddressExist() {
        service.delete(1L);
        verify(repository, times(1)).delete(1L);
    }

}
