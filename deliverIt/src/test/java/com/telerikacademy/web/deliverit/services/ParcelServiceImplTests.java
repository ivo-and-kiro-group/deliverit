package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exceptions.DeletingEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exceptions.LocalDateException;
import com.telerikacademy.web.deliverit.helpers.ParcelHelper;
import com.telerikacademy.web.deliverit.helpers.ShipmentHelper;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.repositories.contracts.ParcelRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ParcelServiceImplTests {

    @Mock
    ParcelRepository repository;

    @InjectMocks
    ParcelServiceImpl service;

    @Test
    public void getById_Should_Call_Repository_When_ParcelExist() {
        var mockParcel = ParcelHelper.createMockParcel();

        when(repository.getById(mockParcel.getId()))
                .thenReturn(mockParcel);

        service.getById(mockParcel.getId());

        verify(repository, times(1)).getById(mockParcel.getId());
    }

    @Test
    public void getById_Should_Throw_When_ParcelDoesntExist() {
        var mockParcel = ParcelHelper.createMockParcel();

        when(repository.getById(mockParcel.getId()))
                .thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getById(1L));
    }

    @Test
    public void getAll_Should_Call_Repository() {

        when(repository.getAll(Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty()))
                .thenReturn(new ArrayList<Parcel>());

        service.getAll(Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());

        verify(repository, times(1)).getAll(Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());
    }

    @Test
    public void getShipmentParcels_Should_Call_Repository() {

        when(repository.getShipmentParcels(1L))
                .thenReturn(new ArrayList<Parcel>());

        service.getShipmentParcels(1L);

        verify(repository, times(1)).getShipmentParcels(1L);
    }

    @Test
    public void create_Should_Call_When_Repository_ShipmentStatusIsPreparing() {
        var mockParcel = ParcelHelper.createMockParcel();
        var mockShipment = ShipmentHelper.createMockShipment();

        mockShipment.setDepartureDate(LocalDate.now().plusDays(2));
        mockShipment.setArrivalDate(LocalDate.now().plusDays(10));

        mockParcel.setShipment(mockShipment);

        service.create(mockParcel);

        verify(repository, times(1)).create(mockParcel);
    }

    @Test
    public void create_Should_Throw_When_ShipmentStatusIsCompleted() {
        var mockParcel = ParcelHelper.createMockParcel();

        mockParcel.getShipment().setDepartureDate(LocalDate.now().minusDays(10));
        mockParcel.getShipment().setArrivalDate(LocalDate.now().minusDays(2));

        assertThrows(LocalDateException.class,
                () -> service.create(mockParcel));
    }


    @Test
    public void create_Should_Throw_When_ShipmentStatusIsOnTheWay(){
        var mockParcel = ParcelHelper.createMockParcel();
        mockParcel.getShipment().setDepartureDate(LocalDate.now());
        mockParcel.getShipment().setArrivalDate(LocalDate.now().plusDays(2));

        List<Parcel> parcels = service.getShipmentParcels(mockParcel.getShipment().getId());
        parcels.add(mockParcel);

        when(repository.getShipmentParcels(mockParcel.getShipment().getId()))
                .thenReturn(parcels);


        assertThrows(LocalDateException.class,
                () -> service.create(mockParcel));
    }

    @Test
    public void update_Should_Call_RepositoryWhenParcelExist() {
        var mockParcel = ParcelHelper.createMockParcel();

        when(repository.update(mockParcel))
                .thenReturn(mockParcel);

        service.update(mockParcel);

        verify(repository, times(1)).update(mockParcel);
    }

    @Test
    public void update_Should_Throw_WhenParcelDoesntExist() {
        var mockParcel = ParcelHelper.createMockParcel();

        when(repository.update(mockParcel))
                .thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.update(mockParcel));
    }

    @Test
    public void delete_Should_Call_Repository() {
        var mockParcel = ParcelHelper.createMockParcel();

        when(repository.getById(mockParcel.getId()))
                .thenReturn(mockParcel);

        when(repository.getShipmentParcels(mockParcel.getShipment().getId()))
                .thenReturn(new ArrayList<Parcel>());

        service.delete(1L);

        verify(repository, times(1)).delete(mockParcel.getId());

    }

    @Test
    public void delete_Should_Throw_When_ParcelDoesntExists() {
        var mockParcel = ParcelHelper.createMockParcel();

        when(repository.getById(mockParcel.getId()))
                .thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.delete(1L));

    }

    @Test
    public void delete_Should_Throw_When_ShipmentStatusOfParcelIsOnTheWay() {
        var mockParcel = ParcelHelper.createMockParcel();
        var anotherParcel = ParcelHelper.createMockParcel();
        mockParcel.getShipment().setDepartureDate(LocalDate.now());
        mockParcel.getShipment().setArrivalDate(LocalDate.now().plusDays(2));

        List<Parcel> parcels = new ArrayList<>();
        parcels.add(anotherParcel);
        mockParcel.getShipment().calcStatus(parcels);

        when(repository.getById(mockParcel.getId()))
                .thenReturn(mockParcel);

        when(repository.getShipmentParcels(mockParcel.getShipment().getId()))
                .thenReturn(parcels);

        assertThrows(DeletingEntityException.class,
                () -> service.delete(1L));

    }
}
